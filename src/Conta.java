public class Conta {

    private String line;
    private String construction;
    private int conta = 0;

    public Conta() {
    }

    public Conta(String line, String costruction) {
        this.line = line;
        this.construction = costruction;
        this.conta++;
    }

    public Conta(Conta c) {
        this.line = c.line;
        this.construction = c.construction;
        this.conta++;
    }

    public Conta(Assicurazione a) {
        this.line = a.getLine();
        this.construction = a.getConstruction();
        this.conta++;
    }

    public void incrementa(){
        conta++;
    }

    public String getLine() {
        return this.line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getCostruction() {
        return this.construction;
    }

    public void setCostruction(String costruction) {
        this.construction = costruction;
    }

    public int getConta() {
        return this.conta;
    }

    public void setConta(int conta) {
        this.conta = conta;
    }
          

    @Override
    public boolean equals(Object o){
        if(this == o){
            return true;
        } else if(o instanceof Conta){
            Conta a = (Conta) o;
            return this.construction.equals(a.getCostruction()) 
                    && this.line.equals(a.getLine());
        } else{
            return false;
        }
    }

    public Conta clone(Conta c){
        return new Conta(c.getLine(), c.getCostruction());
    }

    @Override
    public String toString() {
        return 
            "Line : " + getLine() + System.lineSeparator() +
            "Costruction : " + getCostruction() + System.lineSeparator() +
            "Conta : " + getConta() + System.lineSeparator();
    }


}
