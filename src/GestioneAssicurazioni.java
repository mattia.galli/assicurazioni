import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class GestioneAssicurazioni {
    ArrayList<Assicurazione> lista = new ArrayList<Assicurazione>();

    public void loadCSV(String nomeFile, int i) throws IOException{
    Scanner scanner = new Scanner(new File(nomeFile)).useDelimiter(System.lineSeparator());
        try {
            scanner.nextLine();
            while (scanner.hasNext() && i-- >= 0) {
                final String[] valori = scanner.next().split(",");
                lista.add(new Assicurazione(Double.valueOf(valori[0]), valori[1], valori[2], Double.valueOf(valori[3]), Double.valueOf(valori[4]), Double.valueOf(valori[5]), Double.valueOf(valori[6]), BigDecimal.valueOf(Double.valueOf(valori[7])), BigDecimal.valueOf(Double.valueOf(valori[8])),Double.valueOf(valori[9]), Double.valueOf(valori[10]), Double.valueOf(valori[11]), Double.valueOf(valori[12]), Double.valueOf(valori[13]), Double.valueOf(valori[14]), valori[15], valori[16], Double.valueOf(valori[17]))); 
            }    
        } catch (Exception e) {
          e.printStackTrace();

        } finally{
            scanner.close();
        }
    }

    public void save(String nomeFile){
        try{
            FileOutputStream file = new FileOutputStream(new File(nomeFile));
            ObjectOutputStream object = new ObjectOutputStream(file);
            object.writeObject(lista);
            file.close();
            object.close();
        } catch (Exception e){

        }
    }

    @SuppressWarnings("unchecked")
    public void load(String nomeFile){
        try{
            FileInputStream file = new FileInputStream(new File(nomeFile));
            ObjectInputStream object = new ObjectInputStream(file);
            lista = (ArrayList<Assicurazione>) object.readObject();

        } catch (Exception e){

        }
    }

    public void remove(Assicurazione a){
        lista.remove(a);
    }

    public void sort(){
        Collections.sort(lista);
    }

    public void aggiungi(Assicurazione a){
        lista.add(a.clone());
    }

    public void edit(Assicurazione nuova){
        for(Assicurazione a : lista){
            if(a.ricerca(nuova)){
                lista.remove(a);
                lista.add(nuova);
            }
        }
    }

    @Override
    public String toString(){
        String s = "";
        for(Assicurazione a : lista){
            s += a.toString() + System.lineSeparator();
        }
        return s;
    }

    public ArrayList<Assicurazione> getArrayList(){
        return lista;
    }

    public Assicurazione getAssicurazione(int i){
        return lista.get(i).clone();
    }

    public int getSize(){
        return lista.size();
    }


}
