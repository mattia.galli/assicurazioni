import java.util.ArrayList;

public class GestioneTIV11 {
    private ArrayList<Conta> lista = new ArrayList<>();
    public GestioneTIV11(GestioneAssicurazioni g){
        
        for(Assicurazione a :g.getArrayList()){
            Conta c = new Conta(a);
            if(lista.contains(c)){
                lista.get(lista.indexOf(c)).incrementa();
            } else{
                lista.add(c);
            }
        }
    }

    public String toString(){
        String s = "";
        for(Conta c : lista){
            s += c.toString() + System.lineSeparator();
        }
        return s;
    }

    public int TotaleComplessivo(){
        int conta = 0;
        for (Conta c : lista) {
            conta += c.getConta();
        }
        return conta;
    }

    public int TotaleXLine(String line){
        int conta = 0;
        for (Conta c : lista) {
            if(c.getLine().equals(line))
            conta += c.getConta();
        }
        return conta;
    }

    public int TotaleXConstruction(String cons){
        int conta = 0;
        for (Conta c : lista) {
            if(c.getCostruction().equals(cons))
            conta += c.getConta();
        }
        return conta;
    }
}
