import java.io.Serializable;
import java.math.BigDecimal;

public class Assicurazione implements Serializable, Comparable<Assicurazione>{

    private static final long serialVersionUID = 1L;

    double policyID; 
    String statecode;
    String county;
    double eq_site_limit;
    double hu_site_limit;
    double fl_site_limit;
    double fr_site_limit;
    BigDecimal tiv_2011;
    BigDecimal tiv_2012;
    double eq_site_deductible;
    double hu_site_deductible; 
    double fl_site_deductible; 
    double fr_site_deductible;
    double point_latitude;
    double point_longitude; 
    String  line; 
    String construction; 
    double point_granularity;



    public Assicurazione() {
    }

    public Assicurazione(double policyID, String statecode, String county, double eq_site_limit, double hu_site_limit, double fl_site_limit, double fr_site_limit, BigDecimal tiv_2011, BigDecimal tiv_2012, double eq_site_deductible, double hu_site_deductible, double fl_site_deductible, double fr_site_deductible, double point_latitude, double point_longitude, String line, String construction, double point_granularity) {
        this.policyID = policyID;
        this.statecode = statecode;
        this.county = county;
        this.eq_site_limit = eq_site_limit;
        this.hu_site_limit = hu_site_limit;
        this.fl_site_limit = fl_site_limit;
        this.fr_site_limit = fr_site_limit;
        this.tiv_2011 = tiv_2011;
        this.tiv_2012 = tiv_2012;
        this.eq_site_deductible = eq_site_deductible;
        this.hu_site_deductible = hu_site_deductible;
        this.fl_site_deductible = fl_site_deductible;
        this.fr_site_deductible = fr_site_deductible;
        this.point_latitude = point_latitude;
        this.point_longitude = point_longitude;
        this.line = line;
        this.construction = construction;
        this.point_granularity = point_granularity;
    }

    public double getPolicyID() {
        return this.policyID;
    }

    public void setPolicyID(double policyID) {
        this.policyID = policyID;
    }

    public String getStatecode() {
        return this.statecode;
    }

    public void setStatecode(String statecode) {
        this.statecode = statecode;
    }

    public String getCounty() {
        return this.county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public double getEq_site_limit() {
        return this.eq_site_limit;
    }

    public void setEq_site_limit(double eq_site_limit) {
        this.eq_site_limit = eq_site_limit;
    }

    public double getHu_site_limit() {
        return this.hu_site_limit;
    }

    public void setHu_site_limit(double hu_site_limit) {
        this.hu_site_limit = hu_site_limit;
    }

    public double getFl_site_limit() {
        return this.fl_site_limit;
    }

    public void setFl_site_limit(double fl_site_limit) {
        this.fl_site_limit = fl_site_limit;
    }

    public double getFr_site_limit() {
        return this.fr_site_limit;
    }

    public void setFr_site_limit(double fr_site_limit) {
        this.fr_site_limit = fr_site_limit;
    }

    public BigDecimal getTiv_2011() {
        return this.tiv_2011;
    }

    public void setTiv_2011(BigDecimal tiv_2011) {
        this.tiv_2011 = tiv_2011;
    }

    public BigDecimal getTiv_2012() {
        return this.tiv_2012;
    }

    public void setTiv_2012(BigDecimal tiv_2012) {
        this.tiv_2012 = tiv_2012;
    }

    public double getEq_site_deductible() {
        return this.eq_site_deductible;
    }

    public void setEq_site_deductible(double eq_site_deductible) {
        this.eq_site_deductible = eq_site_deductible;
    }

    public double getHu_site_deductible() {
        return this.hu_site_deductible;
    }

    public void setHu_site_deductible(double hu_site_deductible) {
        this.hu_site_deductible = hu_site_deductible;
    }

    public double getFl_site_deductible() {
        return this.fl_site_deductible;
    }

    public void setFl_site_deductible(double fl_site_deductible) {
        this.fl_site_deductible = fl_site_deductible;
    }

    public double getFr_site_deductible() {
        return this.fr_site_deductible;
    }

    public void setFr_site_deductible(double fr_site_deductible) {
        this.fr_site_deductible = fr_site_deductible;
    }

    public double getPoint_latitude() {
        return this.point_latitude;
    }

    public void setPoint_latitude(double point_latitude) {
        this.point_latitude = point_latitude;
    }

    public double getPoint_longitude() {
        return this.point_longitude;
    }

    public void setPoint_longitude(double point_longitude) {
        this.point_longitude = point_longitude;
    }

    public String getLine() {
        return this.line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getConstruction() {
        return this.construction;
    }

    public void setConstruction(String construction) {
        this.construction = construction;
    }

    public double getPoint_granularity() {
        return this.point_granularity;
    }

    public void setPoint_granularity(double point_granularity) {
        this.point_granularity = point_granularity;
    }

    public Assicurazione policyID(double policyID) {
        this.policyID = policyID;
        return this;
    }

    public Assicurazione statecode(String statecode) {
        this.statecode = statecode;
        return this;
    }

    public Assicurazione county(String county) {
        this.county = county;
        return this;
    }

    public Assicurazione eq_site_limit(double eq_site_limit) {
        this.eq_site_limit = eq_site_limit;
        return this;
    }

    public Assicurazione hu_site_limit(double hu_site_limit) {
        this.hu_site_limit = hu_site_limit;
        return this;
    }

    public Assicurazione fl_site_limit(double fl_site_limit) {
        this.fl_site_limit = fl_site_limit;
        return this;
    }

    public Assicurazione fr_site_limit(double fr_site_limit) {
        this.fr_site_limit = fr_site_limit;
        return this;
    }

    public Assicurazione tiv_2011(BigDecimal tiv_2011) {
        this.tiv_2011 = tiv_2011;
        return this;
    }

    public Assicurazione tiv_2012(BigDecimal tiv_2012) {
        this.tiv_2012 = tiv_2012;
        return this;
    }

    public Assicurazione eq_site_deductible(double eq_site_deductible) {
        this.eq_site_deductible = eq_site_deductible;
        return this;
    }

    public Assicurazione hu_site_deductible(double hu_site_deductible) {
        this.hu_site_deductible = hu_site_deductible;
        return this;
    }

    public Assicurazione fl_site_deductible(double fl_site_deductible) {
        this.fl_site_deductible = fl_site_deductible;
        return this;
    }

    public Assicurazione fr_site_deductible(double fr_site_deductible) {
        this.fr_site_deductible = fr_site_deductible;
        return this;
    }

    public Assicurazione point_latitude(double point_latitude) {
        this.point_latitude = point_latitude;
        return this;
    }

    public Assicurazione point_longitude(double point_longitude) {
        this.point_longitude = point_longitude;
        return this;
    }

    public Assicurazione line(String line) {
        this.line = line;
        return this;
    }

    public Assicurazione construction(String construction) {
        this.construction = construction;
        return this;
    }

    public Assicurazione point_granularity(double point_granularity) {
        this.point_granularity = point_granularity;
        return this;
    }

    @Override
    public String toString() {
        String s = "";
        s +=
            "PolicyID : " + this.getPolicyID() + System.lineSeparator() +
            "Statecode : " + this.getStatecode() + System.lineSeparator() +
            "County : " + this.getCounty() + System.lineSeparator() +
            /* "Eq_site_limit : " + getEq_site_limit() + System.lineSeparator() +
            "Hu_site_limit : " + getHu_site_limit() + System.lineSeparator() +
            "Fl_site_limit : " + getFl_site_limit() + System.lineSeparator() +
            "Fr_site_limit : " + getFr_site_limit() + System.lineSeparator() + */
            "Tiv_2011 : " + getTiv_2011() + System.lineSeparator() + 
            "Tiv_2012 : " + getTiv_2012() + System.lineSeparator() +
            /* "Eq_site_deductible : " + getEq_site_deductible() + System.lineSeparator() +
            "Hu_site_deductible : " + getHu_site_deductible() + System.lineSeparator() +
            "Fl_site_deductible : " + getFl_site_deductible() + System.lineSeparator() +
            "Fr_site_deductible : " + getFr_site_deductible() + System.lineSeparator() +  */
            "Point_latitude : " + getPoint_latitude() + System.lineSeparator() +
            "Point_longitude : " + getPoint_longitude() + System.lineSeparator() +
            "Line : " + this.getLine() + System.lineSeparator() +
            "Construction : " + this.getConstruction() + System.lineSeparator() ;
            /* "Podouble_granularity : " + getPodouble_granularity() + System.lineSeparator(); */
        return s;
    }

    public int compareTo(Assicurazione a){
        return Double.compare(this.getPolicyID(), a.getPolicyID());
    }
    
    
    public Assicurazione clone(){
        return new Assicurazione(policyID, statecode, county, eq_site_limit, hu_site_limit, fl_site_limit, fr_site_limit, tiv_2011, tiv_2012, eq_site_deductible, hu_site_deductible, fl_site_deductible, fr_site_deductible, point_latitude, point_longitude, line, construction, point_granularity);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        else if (o instanceof Assicurazione) {
            Assicurazione a = (Assicurazione) o;
            return a.policyID == policyID 
            && a.getStatecode().equals(statecode) 
            && a.getCounty().equals(county) 
            && eq_site_limit == a.eq_site_limit 
            && hu_site_limit == a.hu_site_limit 
            && fl_site_limit == a.fl_site_limit 
            && fr_site_limit == a.fr_site_limit 
            && tiv_2011 == a.tiv_2011 
            && tiv_2012 == a.tiv_2012 
            && eq_site_deductible == a.eq_site_deductible 
            && hu_site_deductible == a.hu_site_deductible 
            && fl_site_deductible == a.fl_site_deductible 
            && fr_site_deductible == a.fr_site_deductible 
            && point_latitude == a.point_latitude
            && point_longitude == a.point_longitude
            && a.getLine().equals(line) 
            && a.getConstruction().equals(construction) 
            && point_granularity == a.point_granularity;

        }
        else{
            return false;
        }
    }

    public boolean ricerca(Assicurazione a){
        if(this.policyID == a.getPolicyID()){
            return true;
        }
        return false;
    }
}
   
