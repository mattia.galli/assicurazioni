public class App {
    //Fatto in collaborazione con Pasulo Daniele
    public static GestioneAssicurazioni g = new GestioneAssicurazioni();
    public static void main(String[] args) throws Exception {
        g.loadCSV("Dati.csv", 1000);
        //g.save("Dati.bin");
        //g.load("Dati.bin");
        System.out.println(g.getSize());
        GestioneTIV11 t = new GestioneTIV11(g);
        System.out.println(t);
        System.out.println(t.TotaleComplessivo());
        System.out.println(t.TotaleXLine("Residential"));
        System.out.println(t.TotaleXConstruction("Wood"));
    }
}
